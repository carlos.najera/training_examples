class HelloWorld
  def initialize(name)
    @name = name.capitalize
  end
  # working here
  def say_hi
    puts "Hello #{@name}!"
  end

 # Good Bye Method
  def say_bye
    puts "Good-bye #{@name}!"
  end

 #Dalia's Method 
 def say_hi_to_Dalia
   puts "Hello Dalia!"
 end
end
